<?
//require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

// require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");    
// Global $APPLICATION;
// Global $DB;

// //$my_info = $DB->Query("CREATE TABLE IF NOT EXISTS my_test( id INTEGER NOT NULL auto_increment, identity VARCHAR(256) NOT NULL, PRIMARY KEY(id)) ENGINE = InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci;");
// $my_info = $DB->Query('SELECT * FROM my_test');
// //echo json_encode($my_info);
// $my_info = $my_info->GetNext();
// echo($my_info);

// Parse test query string
//CModule::IncludeModule("catalog");
//global $DB;
function convert($size) {
    $unit=array('b','kb','mb','gb','tb','pb');
    return @round($size/pow(1024,($i=floor(log($size,1024)))),2).' '.$unit[$i];
}

function HumanSize($Bytes) {
    $Type=array("", "K", "M", "G", "T", "peta", "exa", "zetta", "yotta");
    $Index=0;
    while($Bytes>=1024) {
        $Bytes/=1024;
        $Index++;
    }
    return("".round($Bytes)." ".$Type[$Index]."b");
}
// $dbOrders = CSaleOrder::GetList(array("ID" => "ASC"), " ");
//$test = isset($_REQUEST['test']) ? $beginDate = $_REQUEST['test'] : 'nothing';

//$rsUser = CUser::GetByID($USER_ID);
//$arUser = $rsUser->Fetch();

$arr = array (
    //'test'=> $test,
    'cpu'=> sys_getloadavg() ,
    'memory_usage'=>convert(memory_get_usage()),
    'disk_use'=> convert(disk_total_space(".")- disk_free_space(".")),
    'disk_total'=>convert(disk_total_space(".")),
    'free_space'=>HumanSize(disk_free_space(".")),
    //'user'=>$arUser
    //'a'=>1,'b'=>2,'c'=>3,'d'=>4,'e'=>5
);
//echo json_encode($results);
header('Content-type: application/json');
echo json_encode($arr);  
?>
