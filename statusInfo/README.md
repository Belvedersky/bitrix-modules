Скрипт возвращает данные типа json состояния системы:
```json
{
  "cpu": [
    0.04,
    0.06,
    0.05
  ],
  "memory_usage": "390.27 kb",
  "disk_use": "5.52 gb",
  "disk_total": "18.46 gb",
  "free_space": "13 Gb"
}
```